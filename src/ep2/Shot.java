
package ep2;


public class Shot extends Sprite {
    
    public Shot(int x, int y) {
        super(x, y);
      
        loadImage("images/shot.gif");
    }
    
    public void move(){
        y = y - 8 ;
    }
    
    public void setX(int x) {
        this.x=x;
    }

    public void setY(int y) {
        this.y=y;
    }
    
}
