package ep2;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 2;
   
    private int speed_x;
    private int speed_y;
    private int life;
    private int bonus;


    protected Shot shot;
    protected Enemy enemy;
    protected List<Shot> shots;
    protected List<Enemy> enemies;

        
    public Spaceship(int x, int y) {
        super(x, y);
        
        initSpaceShip();
        shot = new Shot(0,0);
        enemy = new Enemy(200, 10);
        shots = new ArrayList<>();

        enemies = new ArrayList<>(); 
        life = 6;
        bonus=0;
    }
    
    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus += bonus;
    }
    
    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life -= life;
    }
   
    private void initSpaceShip() {
        
        noThrust();
        
    }
    
    private void noThrust(){
        loadImage("images/spaceship.gif"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship_thrust.gif"); 
    }    

    public void move() {
        
        // Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the spaceship on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the spaceship on the verical axis
        y += speed_y;
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
            thrust();
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }
        
        if (key == KeyEvent.VK_SPACE) {
            
            this.shots.add(new Shot(x+((image.getWidth(null))/2)-(shot.image.getWidth(null)/2), y-20));
            
        }
        
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
}