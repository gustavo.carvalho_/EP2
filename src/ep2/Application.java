package ep2;

import java.awt.EventQueue;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;


public class Application extends JFrame {
    
    JMenuBar bar = new JMenuBar();
    JMenu game = new JMenu("GAME");
    JMenuItem restart = new JMenuItem("RESTART");
    Map map;

    
    public Application() {
        
        setJMenuBar(bar);
        bar.add(game);
        game.add(restart);
        
        restart.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                map.restart();
            }
        });
              
        menu();
        
        //add(new Map());
        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Diamond Dragon");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    
    public void menu(){
        
        ImageIcon starticon = new ImageIcon("images/start.gif");
        JButton startButton = new JButton(starticon);
        startButton.setFont(new Font("Arial Black", Font.PLAIN, 40));
        startButton.setBackground(Color.red);
        startButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                map = new Map();
                add(map);
                startButton.setVisible(false); 
            }
            
        });
        
        add(startButton);
        startButton.setVisible(true);
        
    }
    
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }
}



