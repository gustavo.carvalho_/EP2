
package ep2;

import java.util.Random;

public class Bonus extends Sprite{
    
    private int timeflag=0;
    
    public Bonus(int x, int y) {
        super(x, y);
      
        loadImage("images/bonus.gif");
    }
    
    public void move(){
        setX(-100);
        setY(-100);
    }
    
    public void randPosition(){
        this.setX(randInt(20,780));
        this.setY(randInt(300,600));
    }
    
    public static int randInt(int min, int max) {
    
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
    }
    
}
