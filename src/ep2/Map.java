package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Map extends JPanel implements ActionListener {

    private int SPACESHIP_X = 220;
    private int SPACESHIP_Y = 430;
    private int ENEMY_X=1;
    private int ENEMY_Y=-5;
    private int BONUS_X=-100;
    private int BONUS_Y=-100;
    private Timer timer_map;
    
    private Image background;
    private Image gameover;
    private Image win; 
    private Spaceship spaceship;
    private Enemy enemy;
    private Bonus bonus;
    public int contador=0;
    

    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.gif");
        this.background = image.getImage();
        ImageIcon image2 = new ImageIcon("images/gameover.png");
        this.gameover = image2.getImage();
        ImageIcon image3 = new ImageIcon("images/win.gif");
        this.win = image3.getImage();
        

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        enemy = new Enemy(ENEMY_X, ENEMY_Y);
        bonus = new Bonus(BONUS_X,BONUS_Y);
        initEnemy();


        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
                            
    }
    
    public void restart(){     
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        enemy = new Enemy(ENEMY_X, ENEMY_Y);
        bonus = new Bonus(BONUS_X,BONUS_Y);
        initEnemy();
        
        timer_map.start();
        
    }

    
    public static int randInt(int min, int max) {
    
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
        
        if(spaceship.getLife()<=0){
            drawGameOver(g);
        }
        if(spaceship.getBonus()==5){
            drawWin(g);
        }
        
        draw(g);
        drawLife(g);

        Toolkit.getDefaultToolkit().sync();
    }

    
    private void draw(Graphics g) {
        
        // Draw missil
    	for(int i=0;i<spaceship.shots.size();i++){
            g.drawImage(spaceship.shots.get(i).getImage(), spaceship.shots.get(i).getX(), spaceship.shots.get(i).getY(), this);
        }
        
        // Draw alien
        for(int i=0;i<spaceship.enemies.size();i++){
            g.drawImage(spaceship.enemies.get(i).getImage(), spaceship.enemies.get(i).getX(), spaceship.enemies.get(i).getY(), this);
        }
        
        //
        g.drawImage(bonus.getImage(), bonus.getX(),bonus.getY(), this);
        
        // Draw spaceship
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
       
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
        updateBonus();
        updateSpaceship();
        updateEnemy();
        updateShot();
        colisor();
        
        repaint();
    }
    
    public void colisor() {
        Rectangle formaNave = spaceship.getBounds();
        Rectangle formaEnemy;
	Rectangle formaShot;
        Rectangle formaBonus = bonus.getBounds();
        //colisão shots e inimigo
        for (int i = 0; i < spaceship.shots.size(); i++) {
            
		formaShot = spaceship.shots.get(i).getBounds();

                    for (int j = 0; j < spaceship.enemies.size(); j++) {
                        
			formaEnemy = spaceship.enemies.get(j).getBounds();

			if (formaShot.intersects(formaEnemy)) {
                                spaceship.enemies.get(j).setVisible(false);
                                spaceship.enemies.remove(j);
				spaceship.shots.get(i).setVisible(false);
                                spaceship.shots.remove(i);
                                enemy.setKills(1);
                                contador+=1;
			}
		}
	}
        //colisão inimigo e nave
        for (int i = 0; i < spaceship.enemies.size(); i++) {

		formaEnemy = spaceship.enemies.get(i).getBounds();

		if (formaNave.intersects(formaEnemy)) {
			spaceship.enemies.get(i).setVisible(false);
                        spaceship.enemies.remove(i);
			spaceship.setLife(1);
		}
	}
        
        //colisão bonus e nave
	if (formaNave.intersects(formaBonus)) {
		bonus.move();
        	spaceship.setBonus(1);
	}
	
        
        //
        if(spaceship.enemies.isEmpty()){
                initEnemy();
        }
        
        for (int i = 0; i < spaceship.enemies.size(); i++) {
            if(spaceship.enemies.get(i).getY()>648){
                spaceship.enemies.remove(i);
            }
        }
        
    }
    
    private void drawLife(Graphics g) {

        String messageLife = "LIFE:";
        String messageBonus = "BONUS: " + spaceship.getBonus();
        String messageKills = "KILLS: " + enemy.getKills();
        Font font = new Font("Arial", Font.BOLD, 16);
        FontMetrics metric = getFontMetrics(font);
        

        g.setColor(Color.white);
        g.setFont(font);
        
        if(spaceship.getLife()==6)
        g.drawString(messageLife+"♥♥♥♥♥♥", 0, 13);
        if(spaceship.getLife()==5)
        g.drawString(messageLife+"♥♥♥♥♥", 0, 13);
        if(spaceship.getLife()==4)
        g.drawString(messageLife+"♥♥♥♥", 0, 13);
        if(spaceship.getLife()==3)
        g.drawString(messageLife+"♥♥♥", 0, 13);
        if(spaceship.getLife()==2)
        g.drawString(messageLife+"♥♥", 0, 13);
        if(spaceship.getLife()==1)
        g.drawString(messageLife+"♥", 0, 13);
        if(spaceship.getLife()==0)
        g.drawString(messageLife, 0, 13);
        
        g.drawString(messageBonus, 0, 28);
        g.drawString(messageKills, 0, 43);
        
       
    }
   
    private void drawGameOver(Graphics g) {
        
        for(int i = 0;i<spaceship.enemies.size();i++){
            spaceship.enemies.remove(i);//remover todos os elementos inimigos
        }
        for(int i = 0;i<spaceship.shots.size();i++){
            spaceship.shots.remove(i);//remover todos os elementos inimigos
        }
        bonus.setX(-80);
        bonus.setY(-80);
        
        spaceship.setX(-50);//fazer a nave sumir da tela posicionando-a em outro lugar
        spaceship.setY(-50);//fazer a nave sumir da tela posicionando-a em outro lugar
        
        g.drawImage(this.gameover,200,150, null);//Desenhar o GAMEOVER

        timer_map.stop();
    }
    
    private void drawWin(Graphics g) {
        
        for(int i = 0;i<spaceship.enemies.size();i++){
            spaceship.enemies.remove(i);//remover todos os elementos inimigos
        }
        for(int i = 0;i<spaceship.shots.size();i++){
            spaceship.shots.remove(i);//remover todos os elementos inimigos
        }
        
        bonus.setX(-80);
        bonus.setY(-80);
            
        spaceship.setX(-50);//fazer a nave sumir da tela posicionando-a em outro lugar
        spaceship.setY(-50);//fazer a nave sumir da tela posicionando-a em outro lugar
        
        g.drawImage(this.win,10,10, null);//Desenhar o GAMEOVER

        timer_map.stop();
    }
    
    
    public void initEnemy(){ 
        for (int i = 0; i < 21; i++){
            spaceship.enemies.add(new Enemy(ENEMY_X, randInt(-80,50)));
            ENEMY_X+=35;
        }
        ENEMY_X=0;
    }
    
    private void updateSpaceship() {
        spaceship.move();
    }
    
    private void updateBonus() {
        if(contador>12){
            bonus.randPosition();
            contador=0;
        }else{
            //bonus.move();
        }
    }
    
    private void updateEnemy() {
        
        
        for(int i=0;i<spaceship.enemies.size();i++){
            if(enemy.getKills()<31)
            spaceship.enemies.get(i).move();
            if(enemy.getKills()>30 && enemy.getKills()<65)
            spaceship.enemies.get(i).moveMedium();
            if(enemy.getKills()>64)
            spaceship.enemies.get(i).moveHard();
        }
    }
    
    private void updateShot() {
        
        for(int i=0;i<spaceship.shots.size();i++){
            spaceship.shots.get(i).move();
        }
        
    }

    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }

        
    }
}