
package ep2;

public class Enemy extends Sprite {
      private int timeflag=0;
      private int kills=0;
      
      
    public Enemy(int x, int y) {
        super(x, y);
        loadImage("images/enemy.gif");
    }  
      
    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills += kills;
    }
    
    
    public void move(){
        timeflag++;
        y++;
        if (timeflag < 50) {
            x++;
        } else if (timeflag < 100) {
            x--;
        } else {
            timeflag=0;
        }
    }
    
    public void moveMedium(){
        timeflag++;
        y+=2;
        if (timeflag < 50) {
            x++;
        } else if (timeflag < 100) {
            x--;
        } else {
            timeflag=0;
        }
    }
    
    public void moveHard(){
        timeflag++;
        y+=4;
        if (timeflag < 50) {
            x++;
        } else if (timeflag < 100) {
            x--;
        } else {
            timeflag=0;
        }
    }
    
}
